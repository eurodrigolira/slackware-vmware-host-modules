Este script faz a instalação dos módulos **vmmon** e **vmnet** no **Slackware** com as versões dos kernels mais recentes.

É importante falar que sempre estou usando o **Slackware Current**, então dependendo da sua versão do Slackware pode ser que não sejá necessário fazer esse procedimento. Caso você faça a atualização do kernel será necessário executar o procedimento novamente.

O script automatiza o processo manual de instalação dos módulos, o projeto original pode ser encontrado no link abaixo:

https://github.com/mkubecek/vmware-host-modules

## Instalação

```
git clone https://gitlab.com/eurodrigolira/slackware-vmware-host-modules.git
cd slackware-vmware-host-modules
chmod +x vmware-host-modules.sh
./vmware-host-modules.sh
```
## Outro Sistemas Operacionais

Este script deve funcionar para outros sistemas operacionais também, com exceção da última parte, que reinicia o serviço:
```
if [ -x /etc/init.d/vmware ]; then
  /etc/init.d/vmware start
fi
```
Dependendo do seu sistema operacional o caminho pode ser diferente.

Você pode descobrir o caminho executando o comando abaixo e reiniciar o serviço manualmente.
```
which vmware
```
## Dúvidas e Sugestões

Em caso de dúvidas e sugestões podem entrar em contato comigo através do email ou redes sociais.

Email: eurodrigolira@gmail.com
Telegram: @eurodrigolira
Twitter: @eurodrigolira
Linkedin: https://linkedin.com/in/eurodrigolira
